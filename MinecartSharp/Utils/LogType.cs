﻿
namespace MinecartSharp.Utils
{
    enum LogType
    {
        Info,
        Warning,
        Error,
        Debug
    }
}
